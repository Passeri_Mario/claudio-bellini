<?php include ('header.php'); ?>

<div class="container">

<?php
    for($i = 1; $i < 10; $i++){

       echo "<div class=\"player\">
                <ul>
                    <li class=\"cover\">
                        <img src=\"assets/img/album.webp\"/>
                    </li>
                    <li class=\"info\">
                        <h4>Player Song " . $i ."</h4>
                        <h2>Album 2021</h2>
                                                                 
                        <div class=\"button-items\">
                            <audio id=\"music".$i."\" preload=\"auto\" loop=\"false\">                         
                                <source src=\"assets/audio/".$i.".mp3\" type=\"audio/mpeg\">                            
                            </audio>
            
                            <div class=\"controls\">
                                <svg id=\"play".$i."\" viewBox=\"0 0 25 25\" xml:space=\"preserve\">
                                   <defs><rect x=\"-49.5\" y=\"-132.9\" width=\"446.4\" height=\"366.4\"/></defs>
                                        <g>
                                            <circle fill=\"none\" cx=\"12.5\" cy=\"12.5\" r=\"10.8\"/>
                                            <path fill-rule=\"evenodd\" clip-rule=\"evenodd\" d=\"M8.7,6.9V18c0,0,0.2,1.4,1.8,0l8.1-4.8c0,0,1.2-1.1-1-2L9.8,6.5 C9.8,6.5,9.1,6,8.7,6.9z\"/>
                                        </g>
                                </svg>
        
                                <svg id=\"pause".$i."\" viewBox=\"0 0 25 25\" xml:space=\"preserve\">
                                    <g>
                                      <rect x=\"6\" y=\"4.6\" width=\"3.8\" height=\"15.7\"/>
                                      <rect x=\"14\" y=\"4.6\" width=\"3.9\" height=\"15.7\"/>
                                    </g>
                                </svg> 
                                
                                <div id=\"lecture".$i."\">
                                     <p>En cours de lecture </p>
                                </div>                                                         
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        ";
    }
?>

</div>

<?php include ('footer.php'); ?>

