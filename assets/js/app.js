
window.onload = () => {

    wow = new WOW(
        {
            boxClass:     'wow',      // default
            animateClass: 'animated', // default
            offset:       300,          // default
            mobile:       false,       // default
            live:         true        // default
        }
    )
    wow.init();

    // Bare de progression
    // Ecouteur d'évènement sur scroll

    window.addEventListener("scroll", () => {
        // Calcul de la hauteur "utile" du document
        let hauteur = document.documentElement.scrollHeight - window.innerHeight

        // Récupération de la position verticale
        let position = window.scrollY

        // Récupération de la largeur de la fenêtre
        let largeur = document.documentElement.clientWidth

        // Calcul de la largeur de la barre
        let barre = position / hauteur * largeur

        // Modification du CSS de la barre
        document.getElementById("progress").style.width = barre+"px"
    })

    // Audio footer toogle

    document.getElementById('card').addEventListener('click', function () {
        let el = document.getElementById('card')
        el.classList.toggle("transform");
    });

    document.getElementById('card2').addEventListener('click', function () {
        let el2 = document.getElementById('card2')
        el2.classList.toggle("transform");
    });

// Gestion du rendu de la vidéo

    let videoPlayButton,
        videoWrapper = document.getElementsByClassName('video-wrapper')[0],
        video = document.getElementsByTagName('video')[0],
        videoMethods = {
            renderVideoPlayButton: function() {
                if (videoWrapper.contains(video)) {
                    this.formatVideoPlayButton()
                    video.classList.add('has-media-controls-hidden')
                    videoPlayButton = document.getElementsByClassName('video-overlay-play-button')[0]
                    videoPlayButton.addEventListener('click', this.hideVideoPlayButton)
                }
            },

            formatVideoPlayButton: function() {
                videoWrapper.insertAdjacentHTML('beforeend', '\
                <svg class="video-overlay-play-button" viewBox="0 0 200 200" alt="Play video">\
                    <circle cx="100" cy="100" r="90" fill="none" stroke-width="15" stroke="#fff"/>\
                    <polygon points="70, 55 70, 145 145, 100" fill="#fff"/>\
                </svg>\ ')
            },

            hideVideoPlayButton: function() {
                video.play()
                videoPlayButton.classList.add('is-hidden')
                video.classList.remove('has-media-controls-hidden')
                video.setAttribute('controls', 'controls')
            }
        }

    videoMethods.renderVideoPlayButton()
}


for(let i = 1; i < 10; i++){

    let music = document.getElementById("music"+i);
    let playButton = document.getElementById("play"+i);
    let pauseButton = document.getElementById("pause"+i);

    playButton.onclick = function() {
        music.play();
        document.getElementById("lecture"+i).style.display="block";
    }

    pauseButton.onclick = function() {
        music.pause();
        document.getElementById("lecture"+i).style.display="none";
    }

}
