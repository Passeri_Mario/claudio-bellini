<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Claudio Bellini</title>
    <!-- SEO  -->
    <meta name="description" content="Site web d'un artiste compositeur, souhait d'avoir un site web pour communiquer sur ses réalisations" />
    <meta name="author" content="Passeri Mario" />
    <meta name="copyright" content="Passeri Mario" />
    <meta name="geo.placename" content="Strasbourg, Alsace">
    <meta name="geo.country" content="FR">
    <!-- Informations réseaux sociaux -->
    <meta property="og:title" content="Site web d'un artiste compositeur, souhait d'avoir un site web pour communiquer sur ses réalisations"/>
    <meta property="og:type" content="website" />
    <meta property="og:url" content="https://dwca.mariopasseri.eu/Claudio_Bellini/"/>
    <meta property="og:image" content="https://dwca.mariopasseri.eu/Claudio_Bellini/assets/img/musicien.jpg"/>
    <meta property="og:description" content="Site web d'un artiste compositeur, souhait d'avoir un site web pour communiquer sur ses réalisations"/>
    <!-- pwa -->
    <link rel="manifest" href="manifest.json">
    <meta name="theme-color" content="#a357d9"/>
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="mobile-web-app-capable" content="yes">
    <script type="module" src="pwabuilder-sw-register.js"></script>
    <!-- Favicon IOS -->
    <link rel="apple-touch-icon" sizes="57x57" href="assets/img/favicon/ios/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="assets/img/favicon/ios/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="assets/img/favicon/ios/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="assets/img/favicon/ios/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="assets/img/favicon/ios/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="assets/img/favicon/ios/apple-icon-120x120.png">
    <!-- Favicon Android -->
    <link rel="apple-touch-icon" sizes="48x48" href="assets/img/favicon/android/android-icon-48x48.png">
    <link rel="apple-touch-icon" sizes="72x72" href="assets/img/favicon/android/android-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="96x96" href="assets/img/favicon/android/android-icon-96x96.png">
    <link rel="apple-touch-icon" sizes="144x144" href="assets/img/favicon/android/android-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="192x192" href="assets/img/favicon/android/android-icon-192x192.png">
    <link rel="apple-touch-icon" sizes="512x512" href="assets/img/favicon/android/android-icon-512x512.png">
    <!-- Favicon navigateur -->
    <link rel="icon" type="image/png" sizes="16x16" href="assets/img/favicon/nav/favicon-16x16.png">
    <link rel="icon" type="image/png" sizes="32x32" href="assets/img/favicon/nav/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="assets/img/favicon/nav/favicon-96x96.png">
    <!-- css -->
    <link rel="stylesheet" type="text/css" href="assets/css/normalize.css">
    <link rel="stylesheet" type="text/css" href="assets/css/animate.min.css">
    <link rel="stylesheet" type="text/css" href="assets/css/style.min.css">
</head>
<body>

<!-- barre de progression -->
<div id="progress"></div>

<!-- header -->
<header>
    <img class="art-header" src="assets/img/song.png" alt="Mélodie Claudio Bellini">
    <div class="header-logo-img container">
        <a href="index.php"><img src="assets/img/claudio.png" title="" alt="Logo Claudio Bellini"></a>
        <h1>Claudio Bellini</h1>
    </div>

    <!-- navigation -->
    <nav class="container">
        <ul>
            <li><a href="index.php">Home</a></li>
            <li><a href="song.php">Album</a></li>
            <li><a href="contact.php">Contact</a></li>
        </ul>
    </nav>
</header>
