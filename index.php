<?php include ('header.php'); ?>

<!-- main -->
<main class="container">

    <!-- bloc 1 Artiste compositeur -->
    <div id="home" class="flex-section">
        <p class="p wow fadeInLeft">Après avoir reçu les paroles ou les avoir écrites moi-même,
            je compose la musique qui saura retranscrire l'ambiance du texte.
            Autres possibilités : créer une mélodie sans connaissance préalable des paroles.</p>

        <div class="card wow fadeIn">
            <img src="assets/img/01.jpeg" alt="Artiste compositeur Claudio bellini" title="Artiste compositeur Claudio bellini">
            <h2>Artiste compositeur</h2>
            <p>Mon travail consiste à écrire de nouvelles chansons, mélodies, à les mettre en musique et à les interpréter.</p>
            <a class="button" href="https://fr.wikipedia.org/wiki/Auteur-compositeur">Découvrir</a>
        </div>
    </div>

    <!-- bloc 2 Spectacle de musiciens -->
    <div class="flex-section">
        <div class="grid-container">
            <div class="card item1 wow fadeIn">
                <img src="assets/img/02.jpeg" alt="Spectacle de musiciens Claudio bellini" title="Spectacle de musiciens Claudio bellini">
                <h2>Spectacle de musiciens</h2>
                <p>L’animation est un élément important à ne pas négliger lorsque l’on organise un événement dans votre vie.</p>
                <a class="button" href="https://fr.wikipedia.org/wiki/Concert">Découvrir</a>
            </div>

            <p class="p item2 wow fadeInRight">Artiste reconnu comme tel, je pratique seul ou en groupe une
                activité artistique à titre professionnel à travers des spectacles et représentations artistique.</p>
        </div>
    </div>

    <!-- bloc 3 Compositeur et grand rêveur -->
    <div class="flex-section">
        <h2 class="h2 wow fadeInLeft">Compositeur et grand rêveur.</h2>
        <div class="video">
            <div id="info" class="video-wrapper wow fadeIn">
                <video poster="assets/img/musicien.jpg">
                    <source src="assets/videos/1.mp4">
                    <source src="assets/videos/1.webm">
                    <source src="assets/videos/1.ogg">
                    Votre navigateur ne supporte pas l'élément vidéo.
                </video>

            </div>
        </div>
    </div>

    <!-- bloc 4 Passion éternelle -->
    <div class="flex-section">
        <div class="grid-container">
            <div class="card item1 wow fadeIn">
                <img src="assets/img/03.jpeg" alt="Passion éternelle Claudio bellini" title="Passion éternelle Claudio bellini">
                <h2>Passion éternelle</h2>
                <p>Je crée la musique par mes doux accents d’amour de passionné.</p>
                <a class="button" href="https://fr.wikipedia.org/wiki/Passion">Découvrir</a>
            </div>

            <p class="p wow item2 fadeInRight">Généralement, les personnes qui travaillent sur un projet musical aiment s'exprimer,
                partager des idées et même rêver à voix haute, je suis passionné par ma profession et j'aime partager mes créations.</p>
        </div>
    </div>
</main>

<?php include ('footer.php'); ?>
