
<!-- footer -->
<footer class="container" id="song">
    <h2>Dernières compositions :  <img class="play-song" src="assets/img/song_play.svg" alt="Dernières compositions Claudio Bellini" title="Dernières compositions Claudio Bellini"></h2>

    <!-- bloc audio 1 -->
    <div class="box flex-section">
        <div id="card-container1">
            <div class="wow fadeIn" id="card">
                <div class="front face">
                    <img src="assets/img/footer_1.jpg" alt="Écouter musique n°1" title="Écouter musique n°1"/>
                </div>
                <div id="face1" class="back face">
                    <audio controls>
                        <source src="assets/audio/1.wav" type="audio/wav">
                        <source src="assets/audio/1.mp3" type="audio/mpeg">
                        <source src="assets/audio/1.ogg" type="audio/ogg">
                        Votre navigateur ne supporte pas l'élément audio.
                    </audio>
                </div>
            </div>
        </div>

        <!-- bloc audio 2 -->
        <div id="card-container2">
            <div class="wow fadeIn" id="card2">
                <div class="front face">
                    <img src="assets/img/footer_2.jpeg" alt="Écouter musique n°2" title="Écouter musique n°2"/>
                </div>
                <div id="face2" class="back face">
                    <audio controls>
                        <source src="assets/audio/2.wav" type="audio/wav">
                        <source src="assets/audio/2.mp3" type="audio/mpeg">
                        <source src="assets/audio/2.ogg" type="audio/ogg">
                        Votre navigateur ne supporte pas l'élément audio.
                    </audio>
                </div>
            </div>
        </div>
    </div>

    <p>Après avoir reçu plusieurs prix dans la composition musicale, je compose des musiques envoûtantes qui
        seront émerveiller les âmes les plus sensibles en procurant de la joie et du divertissement au plus grand nombre.
    </p>
</footer>

<!-- js -->
<script src="assets/js/wow.min.js"></script>
<script src="assets/js/app.min.js"></script>
</body>
</html>